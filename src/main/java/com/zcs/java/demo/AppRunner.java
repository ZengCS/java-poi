package com.zcs.java.demo;

import com.zcs.java.demo.utils.POIUtils;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;

public class AppRunner {
    public static void main(String[] args) {
        String fineName = "testPoi.xlsx";
        readExcel(fineName);
    }

    /**
     * 读取Excel文件内容
     */
    private static void readExcel(String excelName) {
        Workbook workbook = null;
        try {
            workbook = POIUtils.createWorkbook(excelName);
            if (workbook == null) {
                System.out.println("创建Workbook失败");
                return;
            }
            // 开始解析
            int numberOfSheets = workbook.getNumberOfSheets();
            if (numberOfSheets <= 0) {
                System.out.println("没有找到Sheet");
                return;
            }
            final int rowOffset = 0;// 跳过行数，如果第一行是表名，或者无用信息，可以将其跳过
            final int columnOffset = 0;// 跳过列数
            for (int i = 0; i < numberOfSheets; i++) {
                System.out.print("【Sheet: " + (i + 1));
                Sheet sheet = workbook.getSheetAt(i);//读取sheet 0

                int firstRowIndex = sheet.getFirstRowNum() + rowOffset;
                int rowCount = sheet.getLastRowNum();
                System.out.print("   firstRow: " + firstRowIndex);
                System.out.println("   rowCount: " + rowCount + "】");
                System.out.println("------------------------------------------------");
                for (int rIndex = firstRowIndex; rIndex <= rowCount; rIndex++) {   //遍历行
                    // System.out.println("ROW --> " + rIndex);
                    Row row = sheet.getRow(rIndex);
                    if (row != null) {
                        int firstCellIndex = row.getFirstCellNum() + columnOffset;
                        int cellCount = row.getLastCellNum();
                        for (int cIndex = firstCellIndex; cIndex < cellCount; cIndex++) {   //遍历列
                            Cell cell = row.getCell(cIndex);
                            if (cell != null) {
                                CellType cellType = cell.getCellType();
                                if (cellType == CellType.NUMERIC) {
                                    System.out.print(cell.getNumericCellValue());
                                    System.out.print("  |  ");
                                } else if (cellType == CellType.STRING) {
                                    System.out.print(cell.getStringCellValue());
                                    System.out.print("  |  ");
                                } else {
                                    String cellString = cell.toString();
                                    if (cellType == CellType.FORMULA) {
                                        try {
                                            System.out.print(cellString + "=" + cell.getNumericCellValue());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            System.out.print(cell.getStringCellValue());
                                        }
                                    } else {
                                        if (cellString != null && cellString.length() > 0) {
                                            System.out.print(cellString);
                                        } else {
                                            System.out.print("   ");
                                        }
                                    }
                                    System.out.print("  |  ");
                                }
                            }
                        }
                    }
                    System.out.println("");
                    System.out.println("------------------------------------------------");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
